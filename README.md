# omok

A rust implementation of the simple board game. Currently on a reduced board size
of 5x5 but will be scaled up later.

## Rules

The game takes place on a 5x5 grid. You can place your marker at any of the positions
on the grid, as long as there isn't another marker already on that position. When you
place five markers in a row, column or diagonal, you win the game
(it's like tic-tac-toe but on a bigger board).
