use crate::board::generate_board;
use crate::board::Board;
use crate::board::Player;
use crate::board::PlayerType;
use rand::Rng;
use crate::rules;
use std::io;


fn random_position() -> usize {
   let pos = rand::thread_rng().gen_range(0..=4);
   pos
}


fn next_move(gameboard: &mut Board, symbol: char, x:usize, y:usize)
       -> Board {
   println!("New move at ({}, {})", x, y);

    gameboard.board[x][y] = symbol;
    let newboard: Board = *gameboard;
    newboard
}

fn player_check(playertype: PlayerType, board: Board) -> (usize, usize) {
    match playertype {
        PlayerType::Human => take_input(board),
        PlayerType::AI => (random_position(), random_position()),
    }
}

fn take_input(board: Board) -> (usize, usize) {
    loop {
        let mut x = String::new(); let mut y = String::new();

        println!("Input x coordinate.");
        io::stdin()
            .read_line(&mut x)
            .expect("Failed to read line");

        let x: usize = match x.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Invalid input; try again.");
                continue
            },
        };
        
        println!("Input y coordinate.");
        io::stdin()
            .read_line(&mut y)
            .expect("Failed to read line");

        let y: usize = match y.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Invalid input; try again.");
                continue
            },
        };

        if (x < board.width()) & (y < board.height()) {
            return (x, y);
        } else {
            println!("Coordinates provided exceeds board size!");
        }
    }

}


fn take_turn(player: Player, gameboard: &mut Board) ->(Board, bool) {
    let (mut x, mut y): (usize, usize) = player_check(player.player_type, *gameboard);
    let mut overlap: bool = rules::overlaps(gameboard, x, y);
    while overlap {
        println!("Invalid position; chosen coordinate is already occupied.");
        (x, y) = player_check(player.player_type, *gameboard);
        overlap = rules::overlaps(gameboard, x, y);
    }
    
    let newboard: Board = next_move(gameboard, player.symbol, x, y);
    let win: bool = rules::check_win(newboard, x, y);
    (newboard, win)
}


pub fn game(players: &[Player; 2], printmode: bool) {
    let mut gameboard = generate_board();
    let mut win: bool = false;
    let mut stalemate: bool = false;
    let printmode = printmode;
    if printmode {
        println!("{:?}", gameboard.board);
    }

    loop {
        for player in players {

            (gameboard, win) = take_turn(*player, &mut gameboard);
            stalemate = rules::check_stalemate(gameboard);
            
            if printmode {
                println!("{:?}", gameboard.board);
            }

            if win {
                println!("player {} wins!", player.symbol);
                break;
            }

            if stalemate {
                println!("Game reached a stalemate.");
                break;
            }

        };
        if win | stalemate {
            break;
        }
    }

}
