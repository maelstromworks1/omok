use std::io::{BufRead, BufReader};
use std::net::{TcpListener, TcpStream};
use std::thread;


fn start_server(port: u32) {
    let listener = TcpListener::bind(format!("localhost:{}", port)).unwrap();
    println!("Server is listening on port {}", port);

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        println!("New connection: {:?}", stream.peer_addr().unwrap());
    }
}


fn handle_client(stream: TcpStream) {
    let mut reader = BufReader::new(&stream);

    loop {
        let mut buffer = String::new();
        let bytes_read = reader.read_line(&mut buffer).unwrap();

        if bytes_read == 0 {
            break;
        }

        println!("Received message: {:?}", buffer.trim());

    }

    println!("Client disconnected: {:?}", stream.peer_addr().unwrap());
}


pub mod board;
pub mod game;
pub mod rules;

fn main() {
    let server_port: u32 = 8080;
    thread::spawn(move || {
        start_server(server_port);
    });

    let player1: board::Player = board::create_player('A', board::PlayerType::Human);
    let player2: board::Player = board::create_player('B', board::PlayerType::AI);
    game::game(&[player1, player2], true);
}
