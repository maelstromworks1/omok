use crate::board::Board;

pub fn overlaps(board: &mut Board, x: usize, y:usize) -> bool {
    
    match board.board[x][y] != '+' {
        true => true,
        false => false
    }
}


fn out_of_bounds(value: i8) -> bool {
    (value < 0) | (value > 4)
}


fn scan(board: Board, x: usize, y:usize, xstep: i8, ystep: i8) -> usize {
    let mut newx: usize = x;
    let mut newy: usize = y;
    let mut chain = 0;
    while board.board[newx][newy] == board.board[x][y] {
        let tempx: i8 = newx as i8 + xstep;
        let tempy: i8 = newy as i8 + ystep;
        if out_of_bounds(tempx) | out_of_bounds(tempy) {
            return chain
        } else {
            newx = tempx as usize;
            newy = tempy as usize;
            chain = chain + 1;
        }
    }
    return chain - 1;
}


fn check_horizontal(board: Board, x: usize, y:usize) -> bool {
    let xstep: i8 = 1;
    let ystep: i8 = 0;

    let left: usize = scan(board, x, y, -xstep, -ystep);
    let right: usize = scan(board, x, y, xstep, ystep);

    if left + right >= 4 {
        return true
    } else {
        return false
    }
}

fn check_vertical(board: Board, x: usize, y:usize) -> bool {
    let xstep: i8 = 0;
    let ystep: i8 = 1;
    
    let above: usize = scan(board, x, y, -xstep, -ystep);
    let below: usize = scan(board, x, y, xstep, ystep);

    if above + below >= 4 {
        return true
    } else {
        return false
    }
}


fn check_diagonal(board: Board, x: usize, y:usize) -> bool {
    let xstep: i8 = 1;
    let ystep: i8 = -1;
    
    let left: usize = scan(board, x, y, -xstep, -ystep);
    let right: usize = scan(board, x, y, xstep, ystep);

    if left + right >= 4 {
        return true
    } else {
        return false
    }
}


fn check_anti_diagonal(board: Board, x: usize, y:usize) -> bool {
    let xstep: i8 = 1;
    let ystep: i8 = 1;
    
    let left: usize = scan(board, x, y, -xstep, -ystep);
    let right: usize = scan(board, x, y, xstep, ystep);

    if left + right >= 4 {
        return true
    } else {
        return false
    }
}


pub fn check_win(board: Board, x: usize, y: usize) -> bool {

    let win: bool = check_horizontal(board, x, y)
                    | check_vertical(board, x, y)
                    | check_diagonal(board, x, y)
                    | check_anti_diagonal(board, x, y);
    
    win
}


fn array_count(board: Board, symbol: char) -> i16 {
    let mut count: i16 = 0;
    for row in board.board {
        for element in row {
            if element == symbol {
                count = count + 1;
            } else {
                continue;
            }
        }
    }

    count
}

pub fn check_stalemate(board: Board) -> bool {
    let count = array_count(board, '+');
    if count <= 0 {
        return true;
    } else {
        return false;
    }
}
