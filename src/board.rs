#[derive(Clone, Copy)]
pub struct Player{
    pub symbol: char,
    pub player_type: PlayerType,
}

pub fn create_player(symbol: char, player_type: PlayerType) -> Player {
    Player {
        symbol: symbol,
        player_type: player_type,
    }
}


#[derive(PartialEq)]
#[derive(Clone, Copy)]
pub enum PlayerType {
    Human,
    AI
}


#[derive(Clone, Copy)]
pub struct Board {
    width: usize,
    height: usize,
    pub board: [[char; 15]; 15],
}


impl Board {
    pub fn width(&self) -> usize {
        self.width
    }

    pub fn height(&self) -> usize {
        self.height
    }

}


pub fn generate_board() -> Board {
    Board {
        width: 15,
        height: 15,
        board: board(),
    }
}


fn board() -> [[char; 15]; 15] {
    
    let board: [[char; 15]; 15] = [['+'; 15]; 15];

    board
}
